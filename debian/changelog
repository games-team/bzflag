bzflag (2.4.26-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Bump Standards-Version to 4.6.2.
  * d/watch: Use tags instead of releases to fix Github scan.
  * Update lintian overrides to new format.

 -- Reiner Herrmann <reiner@reiner-h.de>  Tue, 03 Jan 2023 20:35:12 +0100

bzflag (2.4.24-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
    - Work around an SDL 2.0.20 event handling regression (Closes: #1003837)
  * Bump Standards-Version to 4.6.1.
  * Fix mismatched lintian override.

 -- Reiner Herrmann <reiner@reiner-h.de>  Thu, 09 Jun 2022 11:35:10 +0200

bzflag (2.4.22-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Build with SDL2.
  * Bump Standards-Version to 4.6.0.
  * d/rules:
    - simplify targets
    - drop dh_installchangelogs call as the upstream changelog is autodetected

 -- Reiner Herrmann <reiner@reiner-h.de>  Sun, 29 Aug 2021 19:14:46 +0200

bzflag (2.4.20-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * debian/control:
    - Mark -data package as Multi-Arch: foreign
    - Point Vcs-* fields to salsa
    - Point homepage to https
    - Update debhelper compat to 13
    - Bump Standards-Version to 4.5.0
    - Declare that d/rules requires root
    - Add Pre-Depends for bzflag-server
      (lintian skip-systemd-native-flag-missing-pre-depends)
  * debian/rules:
    - Drop --as-needed linker flag, which is now default
    - Drop unnecessary get-orig-source target
    - Simplify d/rules by dropping unnecessary configure options
  * Use d/clean instead of overriding dh_clean.
  * Install files from debian/tmp instead of the source directory, so
    that dh_missing can detect them.
  * Drop automatically created directories from *.dirs
  * Add lintian-override for detected duplicate word in README.Debian
  * Add upstream metadata.

 -- Reiner Herrmann <reiner@reiner-h.de>  Sat, 16 May 2020 13:33:36 +0200

bzflag (2.4.18-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 2.4.18.
  * Install png icon into hicolor icon directory.

 -- Markus Koschany <apo@debian.org>  Sat, 27 Oct 2018 23:10:21 +0200

bzflag (2.4.16-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 2.4.16.
  * bzflag-server.examples: Install an example server configuration file.
  * wrap-and-sort -sa.
  * Declare compliance with Debian Policy 4.2.1.

 -- Markus Koschany <apo@debian.org>  Mon, 01 Oct 2018 23:40:39 +0200

bzflag (2.4.14-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 2.4.14.
  * Switch to compat level 11.
  * Declare compliance with Debian Policy 4.1.4.
  * Replace dh_systemd_enable with dh_installsystemd.

 -- Markus Koschany <apo@debian.org>  Wed, 09 May 2018 10:03:48 +0200

bzflag (2.4.12-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 2.4.12.
  * Declare compliance with Debian Policy 4.1.1.
  * Remove dh-systemd and dh-autoreconf from Build-Depends.
    No longer necessary with debhelper 10.
  * Clarify the usage of /etc/default/bzflag.
    Thanks to Owen Heisler for the report. (Closes: #879628)
  * Update debian/copyright for new release.

 -- Markus Koschany <apo@debian.org>  Sat, 04 Nov 2017 01:03:45 +0100

bzflag (2.4.10-2) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.
  * Declare compliance with Debian Policy 4.0.0.
  * Use https for Format field.

 -- Markus Koschany <apo@debian.org>  Sat, 24 Jun 2017 15:46:37 +0200

bzflag (2.4.10-1) experimental; urgency=medium

  * Team upload.
  * New upstream version 2.4.10.
  * Drop deprecated bzflag-client.menu file.
  * Update debian/copyright for new release.
  * export DEB_BUILD_MAINT_OPTIONS = hardening=+all.

 -- Markus Koschany <apo@debian.org>  Sun, 14 May 2017 18:13:34 +0200

bzflag (2.4.8-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 2.4.8.
  * Drop 06_PATH_MAX.diff. Apparently PATH_MAX is no longer used.
  * Drop 04_desktop_file_keywords.diff. Fixed upstream.
  * Switch to compat level 10.
  * Make the build reproducible. Thanks to Reiner Herrmann for the report and
    patch. (Closes: #845317)

 -- Markus Koschany <apo@debian.org>  Sun, 04 Dec 2016 00:54:05 +0100

bzflag (2.4.6-1) unstable; urgency=medium

  * Team upload.

  [ Alexandre Detiste ]
  * add a manpage for bzfquery
  * avoid shipping Makefiles in bzflag-data
  * project has moved from SF to GitHub, update watch file

  [ Markus Koschany ]
  * Imported Upstream version 2.4.6. (Closes: #818080)
  * Do not repack the upstream tarball anymore.
  * Declare compliance with Debian Policy 3.9.8.
  * Vcs-Git: Use https.
  * debian/watch: Use version=4.
  * Remove README.source. The upstream tarball is pristine now.
  * Add get-orig-source target.
  * Drop custom debian/gbp.conf file.
  * Drop the following patches because these issues were fixed upstream.
    - 01_repack_no_other.diff
    - 02_man_fixups.diff
    - 03_maxhostnamelen.diff
    - 05_dedup_pt_translations.diff
    - 07_hurd_autotools_support.diff
    - 08_clang_FTBFS.patch
  * Build-Depend on libcpputest-dev for testing purposes.
  * debian/rules: Do not install the plugindocs because copyplugindocs does not
    exist anymore.
  * bzflag-server.manpages: Install bzfquery man page.
  * Export DEB_LDFLAGS_MAINT_APPEND = -Wl,--as-needed to avoid useless
    dependencies.
  * Add bzflag-client.lintian-overrides to silence duplicate word warnings
    that are unwarranted.
  * Update debian/copyright to copyright format 1.0. Update license information
    for new release.
  * Add override for dh_clean and ensure that the game can be built twice in a
    row.

 -- Markus Koschany <apo@debian.org>  Fri, 22 Jul 2016 11:48:35 +0200

bzflag (2.4.2+ds1-7) unstable; urgency=medium

  * Team upload.
  * bzflag-data: Fix typo in short description.
  * Remove Environment and EnvironmentFile lines from service file. The server
    is enabled by default but it will not advertise itself. This behavior can
    be overridden by creating a custom service file in /etc/systemd/system.
    These changes are now documented in README.Debian. The server will also be
    restarted on-failure. (Closes: #807911)
  * debian/rules: export CCACHE_DIR=$(CURDIR)/ccache to prevent a FTBFS due to
    a nonexistent home directory.

 -- Markus Koschany <apo@debian.org>  Mon, 14 Dec 2015 19:33:24 +0100

bzflag (2.4.2+ds1-6) unstable; urgency=medium

  * Team upload.
  * Declare compliance with Debian Policy 3.9.6.
  * Vcs-Browser: Switch to cgit and use https.
  * Fix typos in package description.
    Thanks to Pascal De Vuyst for the report. (Closes: #745296)
  * Add 09_update_portuguese_translation.patch.
    Thanks to Américo Monteiro for the report and patch. (Closes: #756270)
  * Add 08_clang_FTBFS.patch and fix build errors when compiling with clang.
    Thanks to Alexander for the report and patch. (Closes: #753262)
  * Fix Lintian error description-is-pkg-name.
  * Install bzfquery.pl as /usr/games/bzfquery.
    Thanks to Josh Triplett for the report. (Closes: #728167)
  * Add systemd service files and support systemd.
    Thanks to Alexandre for the report and patch. (Closes: #763036)
  * Build --with systemd and build-depend on dh-systemd.

 -- Markus Koschany <apo@debian.org>  Sun, 01 Nov 2015 16:38:10 +0100

bzflag (2.4.2+ds1-5) unstable; urgency=low

  * Team upload.
  * Change Breaks and Replaces relation of bzflag-client to bzflag-data (<<
    2.4.0) because the desktop file has been moved from the bzflag-data to the
    bzflag-client package. Also tighten the dependency on bzflag-data. This
    prevents bzflag-client from failing to upgrade from wheezy to testing.
    Thanks Andreas Beckmann for the report. (Closes: #718009)
  * Remove lintian-override for missing menu icon. The warning is justified.
  * Point to the correct location of bzflag-32x32.xpm in the menu file.
  * debian/rules: Build with --parallel.

 -- Markus Koschany <apo@gambaru.de>  Tue, 17 Sep 2013 20:24:29 +0200

bzflag (2.4.2+ds1-4) unstable; urgency=low

  * Fix FTBFS on hurd-i386 (Closes: #710797):
    + I mistakenly thought that an undefined MAXHOSTNAMELEN was all that was
      causing the FTBFS
    + Add a missing MAXHOSTNAMELEN definition, 03_maxhostnamelen.diff
    + Define the PATH_MAX when it isn't defined (e.g., on Hurd),
      06_PATH_MAX.diff
    + Add support for GNU Hurd to autotools files,
      07_hurd_autotools_support.diff
  * Drop the spelling-error-in-binary teH/the lintian warning against bzfs in
    bzflag-server; it's a lintian bug and only appears randomly

 -- Ryan Kavanagh <rak@debian.org>  Wed, 05 Jun 2013 08:45:47 -0400

bzflag (2.4.2+ds1-3) unstable; urgency=low

  * Various fixups against bzfs(6) and bzw(5) (LP: #1069537)
    thanks to Bjarnig Ingi Gislason; 02_man_fixups.diff
  * Fix FTBFS on GNU Hurd due to undefined MAXHOSTNAMELEN constant,
    03_maxhostnamelen.diff (Closes: #710797)
  * Add keywords to bzflag.desktop file, 04_desktop_file_keywords.diff
  * Deduplicate Portuguese translation, fixes msgfmt errors;
    05_dedup_pt_translation.diff
  * Added a gbp.conf to facilitate building with a repack branch
  * Make VCS fields canonical
  * Drop override against hardening-no-fortify-functions in flagStay.so;
    the lintian warning has mysteriously disappeared
  * Added lintian-override against mispelling 'teH'/'the' in bzfs
    (bzflag-server)

 -- Ryan Kavanagh <rak@debian.org>  Mon, 03 Jun 2013 23:14:02 -0400

bzflag (2.4.2+ds1-2) unstable; urgency=low

  * Uploading to unstable now that wheezy has been released
  * Readd Tim Riker to Uploaders
  * Use proper Breaks/Replaces in bzflag-client against bzflag (Closes: #699707)
  * Install data files under /usr/share/games (Closes: #600307)
  * libglew1.5-dev no longer exists, drop it from Build-Depends
  * Bump Standards-Version to 3.9.4
  * Override false positive of hardening-no-fortify-functions in flagStay.so
    and logDetail.so
  * Update years in copyright file
  * Remove debian/changelog.in, originally used by upstream

 -- Ryan Kavanagh <rak@debian.org>  Sun, 26 May 2013 18:51:55 -0400

bzflag (2.4.2+ds1-1) experimental; urgency=low

  [ Ryan Kavanagh ]
  * New upstream release (Closes: #636685, #649144, LP: #555388)
    + bzflag.desktop is no longer uses the "3DGraphics" category
      (Closes: #588149, LP: #532056)
    + Tarball was repacked to drop many bundled libraries, see copyright file
      and README.source for details.
    + Update autotools files with 01_repack_no_other.diff to no longer deal
      with the dropped bundled libraries.
    + Add a README.source to describe procedure to repack tarball with
      Git/git-buildpackage
  * Set maintainer to Debian Games Team
    + Put packaging in team Git
    + Set Vcs-* tags
  * Bump Standards-Version to 3.9.3
  * Updated copyright file
  * Don't install unneeded README files
  * Don't install the BUGS file, its information is already provided by README
  * Added compat file instead of setting DH_COMPAT=4 in debian/rules
    + Move from an old-style dh_* rules to a dh style rules
    + Bump compat to version from 4 to 9 and bump debhelper build-dependency
      to >= 9
  * Install bzflag-server maps as examples rather than in the root
    documentation directory.
  * Set Debian source format to 3.0 (quilt)
  * Expand and update long descriptions
  * Don't install DejaVu.License in bzflag-data, was getting installed by the
    wildcard used to install font data
  * Override lintian's claimed spelling mistake ment/meant
  * Build-depend on dh-autoreconf and run it from debian/rules
  * Don't create var/run/bzflag with dh_installdirs, instead, create it from
    initscript.
  * The menu file should be in the bzflag-client package, not bzflag-data.
    Moreover, we should also actually install it. (Closes: #499592)
  * Override menu-icon-missing in bzflag-client, it's provided by bzflag-data
  * Update README.Debian to reference the init file and RUN_AT_STARTUP.
  * bzflag-client should suggest bzflag-server, which in turn enhances
    bzflag-client

  [ Karl Goetz ]
  * Move back to a non-native package (Closes: #470743)
  * Enable compression of docs (Closes: #608329)
  * Add ${misc:Depends} for Lintian.
  * bzflag no longer Suggests *and* Depend on bzflag-server
  * Fix typo in bzflag-data's long description.
  * Switch menu section to Games/Action. Thanks blast007 for suggesting the
    section.
  * Remove debian/Makefile.am and debian/Buildstap, we don't them it in Debian
  * Add a watch file
  * Add an init script for the server (disabled by default). (Closes: #489276)
    + Add a defaults file for server options
  * Allow for compressing documentation (Closes: #608329)

  [ Scott Wichser ]
  * Update the rules and control files a bit.
    + Switch B-D to groff from groff-base
    + Use libcurl4-gnutls-dev instead of libcurl3-openssl-dev
    + Add build-dependency on libc-ares-dev
    + Allow libglew1.5-dev
  * Add a Homepage and fix a few Replaces.

 -- Ryan Kavanagh <rak@debian.org>  Tue, 24 Jul 2012 20:47:24 -0400

bzflag (2.0.16.20100405+nmu1) unstable; urgency=low

  * Non-maintainer upload.
  * debian/control: Update build dependencies. (Closes: #629735)

 -- Ansgar Burchardt <ansgar@debian.org>  Tue, 27 Dec 2011 16:57:37 +0100

bzflag (2.0.16.20100405) unstable; urgency=medium

  * upstream update
  * network and plugin fixes
 -- Tim Riker <Tim@Rikers.org>  Mon, 05 Apr 2010 17:21:54 +0000

bzflag (2.0.13.20080902-1) unstable; urgency=medium

  * add Replaces on -data and -client (Closes: #497520)
  * more updates to debian/copyright (See #497434)
 -- Tim Riker <Tim@Rikers.org>  Tue, 02 Sep 2008 20:12:54 +0000

bzflag (2.0.13.20080901-2) unstable; urgency=medium

  * More info in copyright (Closes: #497434)
 -- Tim Riker <Tim@Rikers.org>  Mon, 01 Sep 2008 20:32:16 +0000

bzflag (2.0.13.20080901-1) unstable; urgency=medium

  * Convert to free fonts (Closes: #491984)
  * Split out data file deb (Closes: #491982)
  * Add .desktop file (Closes: #475070, #396080)
  * GCC 4.3 fixes
 -- Tim Riker <Tim@Rikers.org>  Mon, 01 Sep 2008 02:08:51 +0000

bzflag (2.0.10.20071115) unstable; urgency=medium

  * fix network and timing issues on newer hardware
  * fix bashisms in debian/rules (Closes: #439075)
  * stability improvements (Closes: #288684)
  * newer gcc support (Closes: #417128)
  * list server, and map selection fixes (Closes: #326498)
  * memory corruption fixes (Closes: #377116)
  * alsa sound help and fixes (Closes: #381425)
  * include missing man pages (Closes: #408907)
  * upstream release - see ChangeLog
 -- Tim Riker <Tim@Rikers.org>  Thu, 15 Nov 2007 23:10:51 +0000

bzflag (2.0.8.20060605) unstable; urgency=medium

  * work around broken autoconf prefix handling (Closes: #370304)
 -- Tim Riker <Tim@Rikers.org>  Mon, 05 Jun 2006 22:22:43 +0000

bzflag (2.0.8.20060603) unstable; urgency=medium

  * work around broken ccache installs on buildd servers
 -- Tim Riker <Tim@Rikers.org>  Sat, 03 Jun 2006 00:37:11 +0000

bzflag (2.0.8.20060515) unstable; urgency=medium

  * work around broken amd64 compiler (Closes: #323815)
  * upstream release - see ChangeLog
 -- Tim Riker <Tim@Rikers.org>  Mon, 15 May 2006 16:35:37 +0000

bzflag (2.0.6.20060412-1) unstable; urgency=medium

  * Changes from Non-maintainer upload (Closes: #346644, #347986)
  * remove GPL adns depend
  * replace RSA md5 code (Closes: #345837, #343551)
  * GNU/kFreeBSD patch (Closes: #341145)
  * rebuild should fix amd64 compiler trouble (Closes: #323815)
  * upstream release - see ChangeLog (Closes: #345245)
 -- Tim Riker <Tim@Rikers.org>  Wed, 12 Apr 2006 04:09:40 +0000

bzflag (2.0.4.20051017-0.1) unstable; urgency=low

  * Non-maintainer upload.
  * Remove Build-Dependency on xlibs-dev (Closes: #346644).
  * Remove Build-Dependency on libcurl-dev.
  * Not a native package. Fix it. (Closes: #347986).
  * Updated DH_COMPAT to 4.

 -- Amaya Rodrigo Sastre <amaya@debian.org>  Sun, 22 Jan 2006 21:28:47 +0100

bzflag (2.0.4.20051017) unstable; urgency=medium

  * upstream release - see ChangeLog
  * Previous NMU had network trouble. (Closes: #324276)
  * .de locale fix (Closes: #314144)
  * Closes: #301950, #169156, #320268, #323815, #324170
 -- Tim Riker <Tim@Rikers.org>  Mon, 17 Oct 2005 16:34:32 +0000

bzflag (2.0.2.20050318-0.1) unstable; urgency=high

  * Non-maintainer upload.
  * High-urgency upload for RC bugfix.
  * Forward-declare class SortedGState for compatibility with g++-4.0's
    increased strictness; thanks to Matt Kraai for the patch
    (closes: #320268).

 -- Steve Langasek <vorlon@debian.org>  Sat, 13 Aug 2005 01:08:56 -0700

bzflag (2.0.2.20050318) unstable; urgency=medium

  * upstream release - many big fixes - see ChangeLog
  * Release Manager: sarge/unstable upload
  * license fix - Closes: #298600
  * Closes: #292723, #285306
 -- Tim Riker <Tim@Rikers.org>  Fri, 18 Mar 2005 08:40:38 +0000

bzflag (2.0.0.20040117) unstable; urgency=medium

  * upstream release
  * Closes: #249923, #283186, #245829, #285573, #290648
 -- Tim Riker <Tim@Rikers.org>  Mon, 17 Jan 2005 22:59:07 +0000

bzflag (1.10.6.20040516) unstable; urgency=medium

  * upstream release
  * CanNotDuplicate Closes: #120399, #179785, #226826
  * Add menu icon Closes: #194008
 -- Tim Riker <Tim@Rikers.org>  Sat, 16 May 2004 01:10:06 -0500

bzflag (1.10.4.20030124) unstable; urgency=medium

  * Release Manager: 1.10 network api changed, stable/testing/unstable
  * many upstream changes

 -- Tim Riker <Tim@Rikers.org>  Tue, 23 Dec 2003 14:48:09 -0600

bzflag (1.10.2.20031223) unstable; urgency=medium

  * Release Manager: see below, network api changed, stable/testing/unstable
  * 1.10.x bug fixes including server hangs
  * Network fix for game list server access
  * debian/rules fixes Closes: #224307

 -- Tim Riker <Tim@Rikers.org>  Tue, 23 Dec 2003 14:48:09 -0600

bzflag (1.10.0) unstable; urgency=medium

  * Release Manager: see below, network api changed, stable/testing/unstable
  * update to actual release number
  * add missing -sb

 -- Tim Riker <Tim@Rikers.org>  Tue, 09 Dec 2003 23:45:58 -0600

bzflag (1.9.10) unstable; urgency=medium

  * Release Manager: networking changed. update needed in stable and testing
  * many upstream changes
  * license change to LGPL
  * versioning changed Closes: #223098
  * lintian fixes Closes: #215930

 -- Tim Riker <Tim@Rikers.org>  Tue, 09 Dec 2003 01:45:58 -0600

bzflag (1.7g2-1) unstable; urgency=low

  * flatten symlinks Closes: #198137

 -- Tim Riker <Tim@Rikers.org>  Fri, 20 Jun 2003 10:15:54 -0500

bzflag (1.7g2) unstable; urgency=medium

  * automake Closes: #181093, #172306
  * leave changelog Closes: #181259
  * build depend libglu-dev Closes: #187947
  * buildsnap reports correct arch Closes: #181260
  * double doc removed Closes: #181261
  * rules supports debug build Closes: #181262
  * upstream update
 -- Tim Riker <Tim@Rikers.org>  Wed, 18 Jun 2003 12:33:30 -0500

bzflag (1.7g0) unstable; urgency=medium

  * compatibile with newer compilers Closes: #134031
  * removed period from description Closes: #151306
 -- Tim Riker <Tim@Rikers.org>  Sun, 08 Dec 2002 01:37:22 -0700

bzflag (1.7e6) unstable; urgency=medium

  * protocol changes - urgency=medium
  * upstream update, many bug fixes and new features
  * devfs audio support Closes: #144965
  * simplify descriptions Closes: #124473,#124478
 -- Tim Riker <Tim@Rikers.org>  Wed, 19 Jun 2002 00:12:28 -0600

bzflag (1.7e4) unstable frozen; urgency=low

  * include ChangeLog Closes: #113827
  * correct X depend Closes: #104844
  * update list server URL (frozen) Closes: #113826
  * remove explicit c++ depend Closes: #105947
  * Release Manager: 113826, networking bug fixes for stable and woody

 -- Tim Riker <Tim@Rikers.org>  Tue, 27 Nov 2001 03:51:09 -0700

bzflag (1.7e2-1) unstable; urgency=low

  * upload sources as well. Closes: #94433

 -- Tim Riker <Tim@Rikers.org>  Wed, 18 Apr 2001 16:25:24 -0600

bzflag (1.7e2) unstable; urgency=low

  * xlibs-dev build depends. Closes: #89794

 -- Tim Riker <Tim@Rikers.org>  Thu, 22 Feb 2001 16:42:22 -0700

bzflag (1.7d.20000324) unstable frozen; urgency=low

  * CVS Build

 -- Erik J. Bernhardson <journey@jps.net>  Fri, 24 Mar 2000 02:34:56 -0800

bzflag (1.7d-1) unstable frozen; urgency=low

  * License change to GNU GPL.
  * fix menu file; bzflag is in /usr/games/. Closes: #55149
  * recompile to depend on libGL and libGLU. Closes: #55232
  * Release Manager: 55232 is RC, please let this through.

 -- Anand Kumria <wildfire@progsoc.uts.edu.au>  Sat, 15 Jan 2000 19:56:34 +1100

bzflag (1.7c-3) unstable; urgency=low

  * modify the bzfs script to work in all cases. Closes: #54516

 -- Anand Kumria <wildfire@progsoc.uts.edu.au>  Tue, 11 Jan 2000 14:28:57 +1100

bzflag (1.7c-2) unstable; urgency=low

  * The bzfs.6 manpage was also in bzflag. Closes: #54336

 -- Anand Kumria <wildfire@progsoc.uts.edu.au>  Wed,  5 Jan 2000 15:40:52 +1100

bzflag (1.7c-1) unstable; urgency=low

  * Initial Release.
  * Patches, as available on web site, applied

 -- Anand Kumria <wildfire@progsoc.uts.edu.au>  Sun, 12 Dec 1999 14:07:25 +1100

Local variables:
mode: debian-changelog
End:
